const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId: {
		type: String
	},
	cart: [
			{
				productId: {
					type: String,
					required :[true, "UserId is required!"]
				},
				productName: {
					type: String
				},
				size: {
					type: String
				},
				quantity: {
					type: Number,
					required :[true, "Qty is required!"]
				},
				price: {
					type: Number
				}
			}
		],
	totalAmount: {
		type: Number,
		default: 0
	},
	AddedOn: {
		type: Date,
		default: new Date()
	}
})



module.exports = mongoose.model("Order", ordersSchema);