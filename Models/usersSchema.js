const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Your email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
			{
				productId: {
					type: String,
				},
				productName: {
					type: String
				},
				size: {
					type: String
				},
				quantity: {
					type: Number,
				}
			}
		],
	totalAmount: {
		type: Number
	}
})


module.exports = mongoose.model("User", usersSchema);