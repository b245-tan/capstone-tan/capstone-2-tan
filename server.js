const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Require ROUTES
const userRoutes = require("./Routes/userRoutes.js")
const productRoutes = require("./Routes/productRoutes.js")
const orderRoutes = require("./Routes/orderRoutes.js")

const port = 4000;
const app = express();


	mongoose.set('strictQuery', true);

	// [MongoDB Connection]
	mongoose.connect("mongodb+srv://admin:admin@batch245-tan.bh8imqy.mongodb.net/batch245_Capstone_API_Tan?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	let db = mongoose.connection;
	// For error handling
	db.on("error", console.error.bind(console, "Connection Error!"));
	// For validation of the Connection
	db.once("open", () => console.log("We are now connected to the cloud!"));


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routing
app.use("/user", userRoutes)
app.use("/product", productRoutes)
app.use("/order", orderRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}.`))


/*
	Separation of Concerns:
		- Model should be connected to the Controller.
		- Controller should be connected to the Routes.
		- Route should be connected to the Server/Application

		model > controller > route > server
*/