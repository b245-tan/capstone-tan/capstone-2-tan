const jwt = require('jsonwebtoken');

const safe = "BookingAPI";

module.exports.createAccessToken = (user) => {

	// payload
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, safe, {});
}


// VERIFY
module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	// If token received and is not undefined.
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, safe, (error, data) => {
			// If JWT is not valid
			if(error){
				return response.send({auth: "Verification failed."})
			} else {
				next();
			}
		})

	} 
	// If token does not exist
	else {
		return response.send({auth: "User is not authorized."})
	}
}



// DECODE
module.exports.decode = (token) => {
	// If the token received is not undefined
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, safe, (error, data) => {
			if(error) {
				return null;
			}
			else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
	// If the token does not exist (undefined)
	else{
		return null;
	}
}