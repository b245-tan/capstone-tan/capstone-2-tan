const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const orderController = require("../Controllers/orderController.js")


// ROUTES

// Route for creating an order
router.post("/addToCart", auth.verify, orderController.addOrder);

// Route for viewing cart of the user
router.get("/viewCart", auth.verify, orderController.viewCart);

// Route for viewing orders of all users (admin only)
router.get("/viewAll", auth.verify, orderController.viewAllOrders);

// Route for checking out cart of the user
router.get("/checkOut", auth.verify, orderController.checkoutCart);

/*
// Route for editing the content of the cart
router.put("/editCart", auth.verify, orderController.editCart);
*/
module.exports = router;