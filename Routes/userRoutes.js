const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")


// ROUTES

// Route for user registration
router.post("/register", userController.userRegistration);

// This route is for the user authentication/login
router.post("/login", userController.userAuthentication);

// This route is for the getProfile
router.get("/details", auth.verify, userController.getProfile);

// This route is for makeAdmin
router.post("/makeAdmin", auth.verify, userController.makeAdmin);



module.exports = router;