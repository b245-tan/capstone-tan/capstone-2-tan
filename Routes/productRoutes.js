const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const productController = require("../Controllers/productController.js")


// ROUTES

// Route for creating a product (admin only)
router.post("/add", auth.verify, productController.addProduct);

// This route is for retrieving all Active products
router.get("/catalogue", productController.getAllActive);

// This route is for retrieving ALL products (admin only)
router.get("/allProducts", productController.getAll);

// This route is for GETTING the details of a specific product
router.get("/details/:id", productController.getUnit);

// This route is for UPDATING the details of a specific product (admin only)
router.put("/update/:id", auth.verify, productController.updateProduct);

// This route is for ARCHIVING a specific product (admin only)
router.get("/archive/:id", auth.verify, productController.archiveProduct);

// This route is for ARCHIVING a specific product (admin only)
router.get("/activate/:id", auth.verify, productController.activateProduct);


module.exports = router;