const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Order = require("../Models/ordersSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js")


// CONTROLLERS

// Controller to create a user
module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result !== null) {
			return response.send(false)
		} else {
			let newUser = new User({
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)


			})
			newUser.save()

			// Create empty cart for new user
			let newOrder = new Order({
				userId: input.email,
				cart: []
			})	
			newOrder.save()


			.then(save => {
				return response.send(true)
			})
			.catch(error => {
				return response.send(false)
			})
		}
	})
	.catch(error =>{
		return response.send(false)
	})
}



// Controller for User Authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result == null) {
			return response.send(false)
		} else {

			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect == true) {
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send(false)
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})

}



// Controller that will accept the user's id and displays the details of the user
module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	return User.findById(userData._id).then(result => {
		result.password = "";
		return response.send(result);
	})
}



// Controller that will make a user an admin (admin only)
module.exports.makeAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	let input = request.body;

	if(userData.isAdmin == false){
		response.send(false);

	} else if(userData.isAdmin == true){

		User.findOne({email: input.email})
		.then(result => {

			if(result == null) {
				response.send(false);
			}
			else {

				User.findByIdAndUpdate(result._id, {isAdmin: true}, {new: true})

				.then(userResult => {
					return response.send(true);
				})

				.catch(error => {
					return response.send(false);
				})
			}
		})
		.catch(error => {
				return response.send(false);
		})

	}
}