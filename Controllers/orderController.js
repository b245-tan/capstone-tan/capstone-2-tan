const Product = require("../Models/productsSchema.js")
const User = require("../Models/usersSchema.js");
const Order = require("../Models/ordersSchema.js")
const auth = require("../auth.js");

// CONTROLLERS

// Controller to add an order to cart
module.exports.addOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let input = request.body;



	let newOrder = new Order({
		userId: userData.email,
		cart: [
			{
				productId: input.productId,
				size: input.size,
				quantity: input.quantity
			}
		]
	})	


	let isUserUpdated = User.findById(userData._id)
	.then(result => {
		if(result === null){
			return response.send(false)
		} else {

			Product.findById(input.productId)
			.then(order =>{

			if(order == null){
				return response.send(false);
			} else {

				if(userData.isAdmin == true){
					return response.send(false);
				} else {
					let cost = order.price * input.quantity;

					result.orders.productName = order.name;
					result.orders.push({
						productId: input.productId,
						productName: order.name,
						size: input.size,
						quantity: input.quantity,
						price: cost
					})

					// result.totalAmount = input.quantity * order.price;
					result.save();
					response.send(true)
				// .then(save => true)
				// .catch(error => false)
				}
			}
			})
			
		}
	})
	.catch(error => response.send(error))



	let isOrderUpdated = Order.findOne({userId: userData.email})
	.then(result => {
		if(result === null){
			return response.send(false)
		} else {

			Product.findById(input.productId)
			.then(order =>{

			let totalAmount = order.price * input.quantity;

			result.cart.push({
				productId: input.productId,
				productName: order.name,
				size: input.size,
				quantity: input.quantity,
				price: totalAmount
			})
			result.totalAmount = result.totalAmount + totalAmount

			result.save();
			})
		}
	})
	.catch(error => response.send(error))

}



// Controller to view cart of the user
module.exports.viewCart = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin == true){
		return response.send(false)

	}
	else {

		let userCart = User.findById(userData._id)
		.then(result => {

			let specificUserCart = Order.findOne({userId: result.email})
			.then(userCart => {

				let specificUserCartId =Order.findById(userCart._id)
				.then(userResult => response.send(userResult))
			})
		})
	}
}




// Controller to view orders of all usres (admin only)
module.exports.viewAllOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)


	if(userData.isAdmin == false){
		return response.send(false)

	} else if(userData.isAdmin == true){
		Order.find()
		.then(result => {
				return response.send(result);
		})
			
		.catch(error => {
				return response.send(error);
		})
	}
}




// Controller to checkout cart of the user
module.exports.checkoutCart = (request, response) => {
	const userData = auth.decode(request.headers.authorization)


	if(userData.isAdmin == true){
		response.send(false);

	}
	else {


		let userCart = User.findById(userData._id)
		.then(result => {

			let specificUserCart = Order.findOne({userId: result.email})
			.then(userCart => {

				let specificUserCartId = Order.findById(userCart._id)
				.then(userResult => {

					let forPayment = userResult.totalAmount;
					let forCheckout = userResult;

					if(forPayment === 0){
						response.send(false);
					} else {

						let userOrder = User.findById(userData._id)
							.then(result => {
								result.orders = [];
								result.totalAmount = 0;
								result.save();
							})

						let orderCart = Order.findOne({userId: userData.email})
							.then(result => {
								result.cart = [];
								result.totalAmount = 0;
								result.save();
							})
							response.send(true)

						/*response.send(`Order has been made with a total payment of \u20B1${forPayment}! Visit us again!


						[OFFICIAL RECEIPT]

						Purchased on: ${Date()}
						
						${forCheckout}`)*/
					}

				})
			
			})
		
		})

	}


}




/*
// Controller to edit content of cart
module.exports.editCart = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let input = request.body;


	Order.findOneAndUpdate({userId: userData.email, productId:input.productId}, 
		{cart: 
			{productId},
			{productName},
			{size},
			{quantity: input.newQuantity},
			{price: input.newQuantity * product.price},
		}, 
		{new:true})
	.then(result => response.send(result))
	.catch(error => response.send(error))	


}

*/