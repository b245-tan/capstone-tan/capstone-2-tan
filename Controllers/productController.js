const Product = require("../Models/productsSchema.js")
const User = require("../Models/usersSchema.js");
const auth = require("../auth.js");


// Controller to add a new product
module.exports.addProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let input = request.body;

	let newProduct = new Product({
		name: input.name,
		description: input.description,
		price: input.price
	})	


	if(userData.isAdmin == false){
			response.send(false);
	} else if(userData.isAdmin == true){

		let productExists = Product.findOne({name: input.name})
		// product successfully created
		.then(unit =>{
			if(unit == null){
				newProduct.save()
				response.send(true);
			} else {
				response.send(false);
			}
		})

		// product creation failed
		.catch(error => {
			response.send(false);
		})
	}

}



// Controller to Retrieve all Active products
module.exports.getAllActive = (request, response) => {
	Product.find({isActive: true})
	.then(result => {
		return response.send(result);
	})
	// method to capture the error when find method is executed
	.catch(error => {
		return response.send(error);
	})
}

// Controller to Retrieve ALL products
module.exports.getAll = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	
		Product.find()
		.then(result => {
			return response.send(result);
		})
		// method to capture the error when find method is executed
		.catch(error => {
			return response.send(error);
		})

	

}




// Controller that will accept the unit's id and displays its details
module.exports.getUnit = (request, response) => {

	let input = request.params.id;
	
	Product.findOne({_id: input})
	.then(result => {
		if(result == null) {
			return response.send("Product Id is not found!")
		} else {
			return response.send(result) 
		}
	})
	.catch(error => {
		return response.send(error);
	})
}




// Controller to update a product
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	let input = request.body;	


	if(userData.isAdmin == false){
			response.send(false);

	} else if(userData.isAdmin == true){

		let idToBeUpdated = request.params.id;

		Product.findByIdAndUpdate(idToBeUpdated, {
				name: input.name,
				description: input.description,
				price: input.price
		},
		{new: true})

		.then(result => {
			return response.send(true)
		})

		.catch(error => {
			return response.send(false);
		})
	}

}



// Controller that will ARCHIVE a specific product
module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	let idToBeUpdated = request.params.id;	


	if(userData.isAdmin == false){
			response.send(false);

	} else if(userData.isAdmin == true){

		Product.findByIdAndUpdate(idToBeUpdated, {
				isActive: false
		},
		{new: true})

		.then(result => {
			return response.send(true)
		})

		.catch(error => {
			return response.send(error);
		})
	}
}


// Controller that will ACTIVATE a specific product
module.exports.activateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	let idToBeUpdated = request.params.id;	


	if(userData.isAdmin == false){
			response.send(false);

	} else if(userData.isAdmin == true){

		Product.findByIdAndUpdate(idToBeUpdated, {
				isActive: true
		},
		{new: true})

		.then(result => {
			return response.send(true)
		})

		.catch(error => {
			return response.send(error);
		})
	}
}